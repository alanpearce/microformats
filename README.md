# microformats

A Clojure library designed to parse [microformats](http://microformats.org/wiki/microformats2).

## Feedback

This is my first foray into Clojure, if I've done something
non-idiomatically please get in touch.

## Usage

FIXME

## License

Copyright © 2014 Alan Pearce

Distributed under the MIT License.
