(defproject microformats "0.1.0-SNAPSHOT"
  :description "HTML microformats parser"
  :url "https://github.com/alanpearce/microformats"
  :license {:name "MIT License"
            :url "http://opensource.org/licenses/MIT"}
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [enlive "1.1.5"]
                 [clojurewerkz/urly "1.0.0"]]
  :profiles {:dev {:dependencies [[expectations "2.0.9"]
                                  [org.clojure/tools.trace "0.7.8"]]}}
  :plugins [[lein-expectations "0.0.7"]]
  :main microformats.parser)
