(ns microformats.parser-expectations
  (:require [expectations :refer :all]
            [microformats.parser :refer :all]
            [clojure.zip :as z]
            [clojure.tools.trace :refer :all]
            [net.cgrand.enlive-html :refer [html-snippet]]))

(untrace-ns 'microformats.parser)
;(trace-ns 'microformats.parser)

(defn- snippets
  [html]
  (map z/xml-zip (html-snippet html)))

(defn- snippet
  [html]
  (first (snippets html)))

(expect [:location]
        (into [] ((classes-to-props "p-") ["someclass" "p-location" "someotherclass"])))

(expect [:location :name]
        (into [] ((classes-to-props "p-") ["someclass" "p-location" "someotherclass" "p-name"])))

(expect "Foo <strong>bar</strong>"
        (apply str (#'microformats.parser/node-to-html '("Foo " {:tag :strong, :attrs nil, :content ("bar")}))))

(expect "665 3rd St. Suite 207"
        (->> "<p class=\"h-adr\">
<span class=\"p-street-address\">665 3rd St.</span>
<span class=\"p-extended-address\">Suite 207</span>
</p>"
             snippet z/node :content
             (#'microformats.parser/node-to-text)))

(expect {:tag :div :attrs {:class "h-card"}
         :content ["\n"
                   {:tag :p :attrs nil
                    :content [{:tag :a :attrs {:class "h-org"}
                               :content nil}]}
                   "\n"]}
        (z/node (snippet "<div class=\"h-card\">
<p><a class=\"h-org\"></a></p>
</div>")))

(expect "http://example.com"
        (get-base-url (snippet "<head><base href=\"http://example.com\"></head>")))

(expect "/"
        (normalise-url (snippet "<head></head>") "/"))

(expect "http://example.com/"
        (normalise-url (snippet "<head><base href=\"http://example.com\"></head>") "/"))

(expect {:name '("Name")}
        (parse-p (snippet "<p class=\"p-name\"><span class=\"value\">Name</span></p>")))

(expect {:name '("Name")}
        (parse-p (snippet "<p class=\"p-name\"><span class=\"value-title\" title=\"Name\">Not this name</span></p>")))

(expect {:name '("Name Endname")}
        (parse-p (snippet "<p class=\"p-name\"><span class=\"value\">Name</span> (this should not be included) <span class=\"value\">Endname</span></p>")))

(expect {:name '("Example User")}
        (parse-p (snippet "<p class=\"p-name\">Example User</p>")))

(expect {:nickname '("exuser")}
        (parse-p (snippet "<p class=\"p-nickname\">exuser</p>")))

(expect {:name '("Example User")}
        (parse-p (snippet "<img class=\"p-name\" alt=\"Example User\">")))

(expect {:name '("Example User")}
        (parse-p (snippet "<abbr class=\"p-name\" title=\"Example User\">@example</abbr>")))

(expect {:name '("@example")}
        (parse-p (snippet "<abbr class=\"p-name\">@example</abbr>")))

(expect {:name '("Example User")}
        (parse-p (snippet "<data class=\"p-name\" value=\"Example User\"></data>")))

(expect {:name '("Example User")}
        (parse-p (snippet "<data class=\"p-name\">Example User</data>")))

(expect {:name '("")}
        (parse-p (snippet "<br class=\"p-name\"/>")))

(expect {:name '("")}
        (parse-p (snippet "<hr class=\"p-name\"/>")))

(expect {:url '("http://example.com")}
        (parse-u (snippet "<a class=\"u-url\" href=\"http://example.com\">Awesome example website</a>")))

(expect {:photo '("http://example.com/someimage.png")}
        (parse-u (snippet "<img class=\"u-photo\" src=\"http://example.com/someimage.png\">")))

(expect {:photo '("http://example.com/someimage.png")}
        (parse-u (snippet "<area class=\"u-photo\" href=\"http://example.com/someimage.png\"></area>")))

(expect {:photo '("http://example.com/someimage.png")}
        (parse-u (snippet "<object class=\"u-photo\" data=\"http://example.com/someimage.png\"></object>")))

(expect {:photo '("http://example.com/someimage.png")}
        (parse-u (snippet "<abbr class=\"u-photo\" title=\"http://example.com/someimage.png\"></abbr>")))

(expect {:photo '("http://example.com/someimage.png")}
        (parse-u (snippet "<abbr class=\"u-photo\">http://example.com/someimage.png</abbr>")))

(expect {:photo '("http://example.com/someimage.png")}
        (parse-u (snippet "<data class=\"u-photo\" value=\"http://example.com/someimage.png\"></data>")))

(expect {:photo '("http://example.com/someimage.png")}
        (parse-u (snippet "<data class=\"u-photo\">http://example.com/someimage.png</data>")))

(expect {:start '("2012-08-05T14:50")}
        (parse-dt (snippet "<time class=\"dt-start\" datetime=\"2012-08-05T14:50\"></time>")))

(expect {:start '("2012-08-05T14:50")}
        (parse-dt (snippet "<time class=\"dt-start\">2012-08-05T14:50</time>")))

(expect {:start '("2012-08-05T14:50")}
        (parse-dt (snippet "<ins class=\"dt-start\" datetime=\"2012-08-05T14:50\"></ins>")))

(expect {:end '("2012-08-05T18:00")}
        (parse-dt (snippet "<del class=\"dt-end\" datetime=\"2012-08-05T18:00\"></del>")))

(expect {:start '("2012-08-05T14:50")}
        (parse-dt (snippet "<abbr class=\"dt-start\" title=\"2012-08-05T14:50\"></abbr>")))

(expect {:start '("2012-08-05T14:50")}
        (parse-dt (snippet "<abbr class=\"dt-start\">2012-08-05T14:50</abbr>")))

(expect {:start '("2012-08-05T14:50")}
        (parse-dt (snippet "<data class=\"dt-start\" value=\"2012-08-05T14:50\"></data>")))

(expect {:start '("2012-08-05T14:50")}
        (parse-dt (snippet "<data class=\"dt-start\">2012-08-05T14:50</data>")))

(expect {:start '("2012-08-05T14:50")}
        (parse-dt (snippet "<input class=\"dt-start\" value=\"2012-08-05T14:50\">")))

(expect {:start '("2012-08-05T14:50")}
        (parse-dt (snippet "<span class=\"dt-start\">2012-08-05T14:50</span>")))

(expect {:content '({:html "Here is a load of <strong>embedded markup</strong>" :value "Here is a load of embedded markup"})}
        (parse-e (snippet "<div class=\"e-content\">Here is a load of <strong>embedded markup</strong></div>")))

(expect {:author '("http://example.com/a")}
        (parse-rels (snippets "<a rel=\"author\" href=\"http://example.com/a\">author a</a>")))

(expect {:author '("http://example.com/a")}
        (parse-rels (snippets "
<html><head><base href=\"http://example.com\"><body>
<a rel=\"author\" href=\"/a\">author a</a>")))

(expect {:author '("http://example.com/a" "http://example.com/b")}
        (parse-rels (snippets "<a rel=\"author\" href=\"http://example.com/a\">author a</a>
<a rel=\"author\" href=\"http://example.com/b\">author b</a>")))

(expect {:items nil :rels {}}
        (parse ""))

(expect {:items '({:properties {:name ("Example User")} :type ("h-card")}) :rels {}}
        (parse "<div class=\"h-card\"><p class=\"p-name\">Example User</p></div>"))

(expect {:items '({:properties {:name ("Example User")} :type ("h-card")}) :rels {}}
        (parse "<div class=\"h-card\"><p class=\"p-name\">  Example User  </p></div>"))

(expect {:items '({:properties {:name ("Example User")} :type ("h-card")}) :rels {}}
        (parse "  <div class=\"h-card\"><p class=\"p-name\">Example User</p></div>"))

(expect {:items '({:properties {:name ("Example User") :url ("http://example.com")}
                   :type ("h-card")}) :rels {}}
        (parse "<div class=\"h-card\"><a class=\"p-name u-url\" href=\"http://example.com\">Example User</></div>"))

(expect {:items '({:type ("h-card") :properties {:name ("Example User")}}) :rels {}}
        (parse "<p class=\"h-card\">Example User</p>"))

(expect {:items '({:type ("h-card") :properties {:name ("Example User")}}) :rels {}}
        (parse "<p class=\"h-card\">Example<span> </span> User</p>"))

(expect {:items '({:type ("h-card") :properties {:name ("Example User") :url ("http://example.com")}}) :rels {}}
        (parse "<a class=\"h-card\" href=\"http://example.com\">Example User</a>"))

(expect {:items '({:type ("h-card") :properties {:name ("Example User")
                                                 :photo ("http://example.com/me.png")}}) :rels {}}
        (parse "<img class=\"h-card\" alt=\"Example User\" src=\"http://example.com/me.png\"></img>"))

(expect {:items '({:type ("h-card") :properties {:name ("Example User")}}) :rels {}}
        (parse "<abbr class=\"h-card\" title=\"Example User\">Incorrect</abbr>"))

(expect {:items '({:type ("h-card") :properties {:name ("Example User")}}) :rels {}}
        (parse "<p class=\"h-card\"><img alt=\"Example User\"></p>"))

(expect {:items '({:type ("h-card") :properties {:name ("Example User")}}) :rels {}}
        (parse "<p class=\"h-card\"><abbr title=\"Example User\">Wrong </abbr></p>"))

(expect {:items '({:type ("h-card") :properties {:name ("Example User")}}) :rels {}}
        (parse "<div class=\"h-card\"><div><img alt=\"Example User\"></div></div>"))

(expect {:items '({:type ("h-card") :properties {:name ("Example User")}}) :rels {}}
        (parse "<div class=\"h-card\"><p><abbr title=\"Example User\">Wrong</abbr></p></div>"))

(expect {:items '({:type ("h-card") :properties {:photo ("http://example.com/me.png")}}) :rels {}}
        (parse "<object class=\"h-card\" data=\"http://example.com/me.png\"></object>"))

(expect {:items '({:type ("h-card") :properties {:name ("Example User")
                                                 :photo ("http://example.com/me.png")}}) :rels {}}
        (parse "<div class=\"h-card\"><img alt=\"Example User\" src=\"http://example.com/me.png\"></div>"))

(expect {:items '({:type ("h-card") :properties {:photo ("http://example.com/me.png")}}) :rels {}}
        (parse "<div class=\"h-card\"><object data=\"http://example.com/me.png\"></object></div>"))

(expect {:items '({:type ("h-card") :properties {:photo ("http://example.com/me.png")}}) :rels {}}
        (parse "<div class=\"h-card\"><div><object data=\"http://example.com/me.png\"></object></div></div>"))

(expect {:items '({:type ("h-card") :properties {:name ("Example User")
                                                 :photo ("http://example.com/me.png")}}) :rels {}}
        (parse "<div class=\"h-card\"><div><img alt=\"Example User\" src=\"http://example.com/me.png\"></div></div>"))

(expect {:items '({:type ("h-card") :properties {:name ("Example User") :url ("http://example.com")}}) :rels {}}
        (parse "<div class=\"h-card\"><a href=\"http://example.com\">Example User</a></div>"))

(expect {:items '({:type ("h-card") :properties {:name ("Example")
                                                 :url ("http://example.com"
                                                       "http://myblog.com")}})
         :rels {}}
        (parse "<div class=\"h-card\">
<p class=\"p-name\">Example</p>
<a class=\"u-url\" href=\"http://example.com\">My Website</a>
<a class=\"u-url\" href=\"http://myblog.com\">My Blog</a>
</div>"))

(expect {:items '({:type ("h-card") :properties {:name ("Example User")
                                                 :url ("http://example.com"
                                                       "http://myblog.com")}})
         :rels {}}
        (parse "<div class=\"h-card\">
<a class=\"u-url p-name\" href=\"http://example.com\">Example User</a>
<a class=\"u-url\" href=\"http://myblog.com\">My Blog</a>
</div>"))

(expect {:items '({:type ("h-card")
                   :properties {:name ("Example")
                                :tel ("01234567890")}})
         :rels {}}
        (parse "<div class=\"h-card\">
<p class=\"p-name\">Example</p>
<p class=\"p-tel\">01234567890</p>
<br class=\"p-tel\">
</div>"))

(expect
 {:items '({:type ("h-adr"),
            :properties {:street-address ("665 3rd St."),
                         :extended-address ("Suite 207"),
                         :locality ("San Francisco"),
                         :region ("CA"),
                         :postal-code ("94107"),
                         :country-name ("U.S.A."),
                         :name ("665 3rd St. Suite 207 San Francisco, CA 94107 U.S.A.")}})
  :rels {}}
 (parse "<p class=\"h-adr\">
<span class=\"p-street-address\">665 3rd St.</span>
<span class=\"p-extended-address\">Suite 207</span>
<span class=\"p-locality\">San Francisco</span>,
<span class=\"p-region\">CA</span>
<span class=\"p-postal-code\">94107</span>
<span class=\"p-country-name\">U.S.A.</span>
</p>"))

(expect {:items '({:type ("h-card" "h-org")
                   :properties {:name ("Example")}})
         :rels {}}
        (parse "<p class=\"h-card h-org\">Example</p>"))

(expect {:items '({:type ("h-card")
                   :properties {:name ("John Doe")
                                :org ({:value "Example"
                                       :type ("h-card" "h-org")
                                       :properties {:name ("Example")}})}})
         :rels {}}
        (parse "<div class=\"h-card\">
<span class=\"p-name\">John Doe</span>
<span class=\"p-org h-card h-org\">Example</span>
</div>"))

(expect {:items '({:type ("h-card") :properties {:name ("Example User") :url ("http://example.com/")}}) :rels {}}
        (parse "<html><head><base href=\"http://example.com\"></head><body>
<div class=\"h-card\"><a class=\"u-url\" href=\"/\">Example User</a></div></body></html>"))

(expect {:items '({:type ("h-card") :properties {:name ("Example User") :url ("http://example.com/")}}) :rels {}}
        (parse "<html><head><base href=\"http://example.com\"></head><body>
<div class=\"h-card\"><a href=\"/\">Example User</a></div></body></html>"))

(expect {:items '({:type ("h-card") :properties {:name ("Example User")
                                                 :photo ("http://example.com/me.png")}}) :rels {}}
        (parse "<html><head><base href=\"http://example.com/\"></head><body>
<div class=\"h-card\"><img alt=\"Example User\" src=\"me.png\"></div>
</body></html>"))

(expect {:items '({:type ("h-card") :properties {:name ("Example User")
                                                 :photo ("http://example.com/me.png")}}) :rels {}}
        (parse "<html><head><base href=\"http://example.com/\"></head><body>
<div class=\"h-card\"><img class=\"u-photo\" alt=\"Example User\" src=\"me.png\"></div>
</body></html>"))

(expect {:items '({:type ("h-card") :properties {:name ("Example User")
                                                 :photo ("http://not-example.com/me.png")}}) :rels {}}
        (parse "<html><head><base href=\"http://example.com/\"></head><body>
<div class=\"h-card\"><img class=\"u-photo\" alt=\"Example User\" src=\"me.png\"></div>
</body></html>" "http://not-example.com/"))

(expect {:items '({:type ("h-card"),
                   :properties {:photo ("http://blog.mozilla.org/press/files/2012/04/mitchell-baker.jpg"),
                                :url ("http://blog.lizardwrangler.com/" "https://twitter.com/MitchellBaker"),
                                :name ("Mitchell Baker"),
                                }})
                 :rels {}}
        (parse "<div class=\"h-card\">
        <img class=\"u-photo\" src=\"http://blog.mozilla.org/press/files/2012/04/mitchell-baker.jpg\"/>
        <a class=\"p-name u-url\" href=\"http://blog.lizardwrangler.com/\">Mitchell Baker</a>
        <a class=\"u-url\" href=\"https://twitter.com/MitchellBaker\">MitchellBaker</a>
        </div>"))

(expect {:items '({:type ("h-entry"),
                   :properties {:name ("microformats.org at 7"),
                                :url ("http://microformats.org/2012/06/25/microformats-org-at-7")}})
         :rels {}}

        (parse "<div class=\"h-entry\">
  <p class=\"p-name\">microformats.org at 7</p>
  <p class=\"u-url\">
  <span class=\"value\">http://microformats.org/</span> -
  <span class=\"value\">2012/06/25/microformats-org-at-7</span>
  </p>

  </div>" "http://example.com/"))
